var minifyCSS = require('gulp-minify-css'),
    uglify = require('gulp-uglify'),
    notify = require('gulp-notify'),
    concat = require('gulp-concat'),
    gulp = require('gulp');

gulp.task('vendorjs', function () {
    gulp.src(['vendors/js/object.traverse.js'])
        .pipe(concat('vendor.js'))
        .pipe(uglify({ compress: true }))
        .pipe(gulp.dest('public/'))
        .pipe(notify('Vendor scripts are builded!'));
});

gulp.task('vendorcss', function () {
    gulp.src(['vendors/css/reset.css'])
        .pipe(concat('vendor.css'))
        .pipe(minifyCSS({ compress: true }))
        .pipe(gulp.dest('public/'))
        .pipe(notify('Vendor styles are builded!'));
});

gulp.task('clientjs', function () {
   gulp.src(['assets/js/helpers.js', 'assets/js/TreePicker.js', 'assets/js/run.js'])
       .pipe(concat('client.js'))
       .pipe(uglify({ compress: true }))
       .pipe(gulp.dest('public/'))
       .pipe(notify('Client scripts are builded!'));
});

gulp.task('clientcss', function () {
    gulp.src(['assets/css/styles.css'])
        .pipe(concat('client.css'))
        .pipe(minifyCSS({ compress: true }))
        .pipe(gulp.dest('public/'))
        .pipe(notify('Client styles are builded!'));
});

gulp.task('default', ['vendorcss', 'clientcss', 'vendorjs', 'clientjs']);
gulp.watch(
    ['vendors/js/*.js', 'vendors/css/*.css', 'assets/css/*.css', 'assets/js/*.js'],
    ['vendorjs', 'vendorcss', 'clientcss', 'clientjs']
);

