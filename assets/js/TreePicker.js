(function (win, doc) {

	function TreePicker (data) {
		this.pieceOfTree = win.helpers.getTemplateById('tree-list');
		this.currentElement = this.pieceOfTree.querySelector('ul');
		this.dialogElem = doc.querySelector('dialog');
		this.currentDepth = 0;
		Object.traverse(data, this.createTree, this);

		return this;
	}

	TreePicker.prototype.init = function (selector) {
		this.renderTo(selector);
		win.helpers.on(doc.querySelector('.toggle-dialog'), 'click', this.toggleShowDialog.bind(this));
		win.helpers.on(this.dialogElem, 'click', this.clickOnDialog);
	};

	TreePicker.prototype.toggleShowDialog = function () {
		var dialog = this.dialogElem;

		if (dialog.open) {
			dialog.close();
		} else {
			dialog.showModal();
		}
	};

	TreePicker.prototype.clickOnDialog = function (event) {
		var target = event.target,
			tagName = target.tagName;

		if (target.classList.contains('close')) {
			this.close();
		}

		if (tagName === 'H3' || tagName === 'LI') {
			Array.from(this.querySelectorAll('.active')).forEach(function (element) {
				element.classList.remove('active')
			});
			target.classList.add('active');
		}

		if (tagName === 'H3') {
			target.parentElement.nextElementSibling.classList.toggle('show-list');
		}
	};

	TreePicker.prototype.createTree = function (node, value, key, path, depth) {
		var depthBetween = Math.abs(this.currentDepth - depth),
			isObjOrArray = win.helpers.isObjectOrArray(value),
			listItem = doc.createElement('li'),
			higherThan = this.currentDepth > depth,
			i = 0, deepUl, tpl;

		if (higherThan) {
			for (; i < depthBetween * 2; i++) {
				this.currentElement = this.currentElement.parentNode;
			}

			if (isObjOrArray) {
				tpl = win.helpers.getTemplateById('tree-list');
				tpl.querySelector('header h3').textContent = key;
				listItem.appendChild(tpl);
			}
		} else {
			if (isObjOrArray) {
				tpl = win.helpers.getTemplateById('tree-list');
				tpl.querySelector('header h3').textContent = key;
				deepUl = tpl.querySelector('ul');
				listItem.appendChild(tpl);
				this.currentElement.appendChild(listItem);
				this.currentElement = deepUl;
			}
		}

		if (!isObjOrArray) {
			this.renderListItem(listItem, key + ': ' + value);
		}

		this.currentDepth = depth;
	};

	TreePicker.prototype.renderTo = function (selector) {
		doc.querySelector(selector).appendChild(this.pieceOfTree);
	};

	TreePicker.prototype.renderListItem = function (listItem, data) {
		listItem.textContent = data;
		this.currentElement.appendChild(listItem);
	};

	win.TreePicker = TreePicker;

}(window, document));