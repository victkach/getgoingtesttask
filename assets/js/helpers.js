(function (win, doc) {

	function isObjectOrArray (dataType) {
		var type = Object.prototype.toString.call(dataType).slice(8, -1);

		return type === 'Object' || type === 'Array';
	}

	function on (obj, eventType, callback) {
		obj.addEventListener(eventType, callback, false);
	}

	function off (obj, eventType, callback) {
		obj.removeEventListener(eventType, callback);
	}

	function getTemplateById (id) {
		var tpl = doc.getElementById(id).content;

		return doc.importNode(tpl, true);
	}

	win.helpers = {
		getTemplateById: getTemplateById,
		isObjectOrArray: isObjectOrArray,
		off: off,
		on: on
	};

}(window, document));